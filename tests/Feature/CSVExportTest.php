<?php

namespace Tests\Feature;

use League\Csv\Reader as CsvReader;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CSVExportTest extends TestCase
{
    /**
     * @test
     */
    public function apiRouteIsFound()
    {
        $this->patch('api/csv-export')
            ->assertStatus(302);
    }

}
