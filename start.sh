#!/bin/bash

cp .env.example .env

composer install

npm i && npm run dev

php artisan key:generate

php artisan serve --port=8085

