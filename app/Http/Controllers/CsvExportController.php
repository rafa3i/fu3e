<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\Contracts\ExportServiceContract;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvExportController extends Controller {

    private $exportService;

    /**
     * CsvExport constructor.
     * @param $exportService
     */
    public function __construct(ExportServiceContract $exportService)
    {
        $this->exportService = $exportService;
    }

    /**
     * Converts the user input into a CSV file and streams the file back to the user
     * @param Request $request
     * @return StreamedResponse
     */
    public function convert(Request $request)
    {
        $validatedData = $request->validate([
            'data' => 'required',
        ]);

        $rows = $validatedData['data']['rows'];
        $columns = array_column($validatedData['data']['columns'], 'key', 'value');

        $callback = $this->exportService->convert($rows, $columns);

        return response()->stream($callback, 200, $this->exportService->getHeader())->sendContent();
    }
}
