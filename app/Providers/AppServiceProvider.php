<?php

namespace App\Providers;

use App\Services\Contracts\ExportServiceContract;
use App\Services\CSVExportService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ExportServiceContract::class, function($app) {
            return new CSVExportService();
        });
    }
}
