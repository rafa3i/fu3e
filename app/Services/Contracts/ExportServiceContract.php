<?php

namespace App\Services\Contracts;

interface ExportServiceContract
{
    public function convert(array $rows, array $columns);
    public function getHeader();
}
