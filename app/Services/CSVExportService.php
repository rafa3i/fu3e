<?php

namespace App\Services;

use App\Services\Contracts\ExportServiceContract;

class CSVExportService implements ExportServiceContract
{
    /**
     * @param array $rows
     * @param array $columns
     * @return \Closure
     */
    public function convert(array $rows, array $columns)
    {
        return function() use ($rows, $columns) {

            $file = fopen('php://output', 'w');
            fputcsv($file, $columns, ',', '"');

            foreach($rows as $row) {

                fputcsv($file, array_values($row), ',', '"');
            }
            fclose($file);
        };
    }

    /**
     * @return string[]
     */
    public function getHeader(): array
    {
        return [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];
    }
}
